package deviser.com.testing_project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Asking_Multiple_Permissions extends AppCompatActivity {

    TextView message;
    final int PERMISSION_ALL = 101;
    String[] PERMISSIONS = {
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.WRITE_CONTACTS,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    ArrayList<String> missedPermissions = new ArrayList<>();
    ArrayList<String> grantedPermissions = new ArrayList<>();
    ArrayList<String> neededPermissions = new ArrayList<>();
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asking__multiple__permissions);

        message = findViewById(R.id.message);



        /*View v = LayoutInflater.from(Asking_Multiple_Permissions.this).inflate(R.layout.progress_bar_spinner,null);
        TextView msz = v.findViewById(R.id.progresbar_text);
        msz.setText("Please wait...");
        AlertDialog.Builder ab=new AlertDialog.Builder(Asking_Multiple_Permissions.this);
        ab.setView(v);
        ab.setCancelable(false);
        alertDialog = ab.create();
        alertDialog.show();
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(alertDialog!=null && alertDialog.isShowing() ){
                    alertDialog.dismiss();
                    message.setText("Dialog Closed");
                }
            }
        },5000);*/

    }

    public boolean checkPermissions() {
        for (String permission : PERMISSIONS) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




    public void requestMultiplePermissions(){
        if(!neededPermissions.isEmpty()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(neededPermissions.toArray(new String[neededPermissions.size()]), PERMISSION_ALL);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);

            if (requestCode == PERMISSION_ALL) {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                       // missedPermissions.add(permissions[i]);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if(shouldShowRequestPermissionRationale(permissions[i])){
                                new AlertDialog.Builder(this)
                                        .setMessage("Your error message here")
                                        .setPositiveButton("Allow", (dialog, which) -> requestMultiplePermissions())
                                        .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                                        .create()
                                        .show();
                            }
                            return;
                        }
                    } else {
                        //grantedPermissions.add(permissions[i]);
                        Toast.makeText(getApplicationContext(),"All Permissions Granted",Toast.LENGTH_SHORT).show();
                    }
                }

                if(!missedPermissions.isEmpty()){

                }
        }
    }

    public void openBottomModalSheetDialogFragment(View v){

        FragmentManager fragmentManager=getSupportFragmentManager();
        BottomSheet_DialogFragment bottomSheet_dialogFragment =new BottomSheet_DialogFragment();
        bottomSheet_dialogFragment.show(fragmentManager,"");
    }

    public void showPermissionStatus(View v){
        AlertDialog.Builder ab=new AlertDialog.Builder(Asking_Multiple_Permissions.this);
        ab.setTitle("Response");
        ab.setMessage("Granted Permissions: "+grantedPermissions.toString()+"\n"+"Missed Permissions: "+missedPermissions.toString());
        ab.setCancelable(false);
        ab.setPositiveButton("Ok", (dialog, which) -> dialog.dismiss());
        ab.show();
    }
}